
filetype off
set nocp
call plug#begin('~/.vim/plugged')
Plug 'junegunn/vim-easy-align'
Plug 'honza/vim-snippets'
Plug 'SirVer/ultisnips'
Plug 'scrooloose/nerdtree'
Plug 'tpope/vim-fireplace'
Plug 'flazz/vim-colorschemes'
call plug#end()
filetype  plugin indent on                 " required
syntax on

runtime! debian.vim

"set background=dark

set showcmd		" Show (partial) command in status line.
set showmatch		" Show matching brackets.
"set ignorecase		" Do case insensitive matching
"set smartcase		" Do smart case matching
"set incsearch		" Incremental search
set autowrite		" Automatically save before commands like :next and :make
set hidden		" Hide buffers when they are abandoned
"set mouse=a		" Enable mouse usage (all modes)

" Source a global configuration file if available
"if filereadable("/etc/vim/vimrc.local")
"  source /etc/vim/vimrc.local
"endif

set number relativenumber
colorscheme wintersday
highlight Normal ctermfg=grey ctermbg=black
inoremap jk <ESC>
